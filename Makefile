CC=gcc
PARAMS=-lm -Wall -g
FOLDER=build/

build_client: cd client

build_server: cd serveur

client: mainClient.c $(FOLDER)client.o
	$(CC) -o client $^ $(PARAMS)

serveur: mainServeur.c $(FOLDER)serveur.o
	$(CC) -o serveur $^ $(PARAMS)

$(FOLDER)client.o: client.c
	$(CC) -o $@ -c $< $(PARAMS)

$(FOLDER)serveur.o: serveur.c
	$(CC) -o $@ -c $< $(PARAMS)

cd:
	mkdir -p $(FOLDER)
	cd $(FOLDER)

clean:
	mkdir -p $(FOLDER)
	rm -r $(FOLDER)
	rm -f client serveur

runAll:
	./serveur & sleep 1 && ./client
