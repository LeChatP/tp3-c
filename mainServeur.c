#define _GNU_SOURCE

#define M_GET 1
#define M_POST 2
#define M_PUT 3
#define M_PATCH 4
#define M_DELETE 5
#define M_TRACE 6
#define M_OPTIONS 7
#define M_CONNECT 8

#define T_HTML 1
#define T_JPG 2
#define T_PNG 3
#define T_ICO 4
#define T_GIF 5

#include "serveur.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <stdint.h>

extern int errno ;
int envoyerReponse200HTML(char *nomFichier);


size_t taille_fichier(char *filename);
int extraitFichier(char *requete, char *tableauNomFichier, int tailleTableauNomFichier);
int analyseMethode(const char *requete);

char *build_message(const char *lines[],int nblines);
char *build_line(const char* line);

int envoyerHeaderReponse(const char *code,const char *type,const unsigned long size);
int envoyerContenuFichierTexte(char *nomFichier);
int envoyerContenuFichierBinaire(char *nomFichier);
int envoyerContenuFichier(char *nomFichier,int binary);

int envoyerReponse200HTML(char *nomFichier);
int envoyerReponse404HTML(char *nomFichier);
int envoyerReponse500HTML(const char *message);

int envoyerReponse200JPG(char *nomFichier);
int envoyerReponse200ICO(char *nomFichier);

int envoyerReponse200Texte(char *nomFichier, char *mimeType);
int envoyerReponse200Binaire(char *nomFichier, char *mimeType);

int analyseExtension(const char *);

int main() {

  Initialisation();

  while (1) {
      AttenteClient();
      ssize_t size = 0;
      char **request =NULL; //la requete est vide
      char *buf = NULL;
      do{ 
        buf=Reception(); //on place la reception dans la case
        if(buf == NULL) goto free_error;
        request = reallocarray(request,size+1,sizeof(char*)); //on ajoute une ligne dans la requete
        request[size] = buf; // on place le pointeur sur la case
        size++; // il y a maintenant une ligne de plus
        //tant que la chaine recue n'est pas vide : "\r\n" on continue a récupérer la requete
      }while(*(request[size-1]) != '\r' && *(request[size-1]+1) != '\n');
      free(request[size-1]);// la dernière chaine récupérée ne sert donc a rien, on peut la vider
      //request[size-1]= NULL;// elle nous permettra de délimiter le tableau de chaines de caractères.
      char fichier[FILENAME_MAX];
      switch (analyseMethode(request[0])) // on analyse la première ligne
      {
        case M_GET:
            extraitFichier(request[0],fichier,FILENAME_MAX);
            if(!access(fichier,F_OK)){
                switch (analyseExtension(fichier))
                {
                case T_JPG:
                  envoyerReponse200JPG(fichier);
                  break;
                case T_GIF:
                  envoyerReponse200Binaire(fichier,"image/gif");;
                  break;
                case T_ICO:
                  envoyerReponse200ICO(fichier);
                  break;
                case T_HTML:
                default:
                  envoyerReponse200HTML(fichier);
                  break;
                }
                
            }else{
                envoyerReponse404HTML(fichier);
            }
            break;
        default:
            envoyerReponse500HTML("Le serveur ne reponds pas a ce type de requete");
            break;
      }
      free_error:
      for(int i = 0 ; i<size-1; i++){
        if(request[i]!=NULL)free(request[i]);
      }
      free(request);
      TerminaisonClient();
  }

  return 0;
}

int analyseExtension(const char *filename){
  char *buf = strrchr(filename,'.');
  if(!strcasecmp(buf,".html")) return T_HTML;
  else if(!strcasecmp(buf,".jpg") || !strcasecmp(buf,".jpeg")) return T_JPG;
  else if(!strcasecmp(buf,".png")) return T_PNG;
  else if(!strcasecmp(buf,".ico")) return T_ICO;
  else if(!strcasecmp(buf,".gif")) return T_GIF;
  else return 0;
}

//retourne 0 si la méthode n'existe pas ou est mal formée
//retourne 1 si GET, 2 si POST, 3 si PUT, 4 si PATCH, 5 si DELETE, 6 si TRACE, 7 si OPTIONS, 8 si CONNECT
int analyseMethode(const char *requete){
    int ret = 0; //compteur de méthodes, et valeur de retour
    switch (*requete) // Optimisation : On cherche si le premier caractère correspond
    {
    case 'C': // CONNECT
        if(strncmp(requete+1,"ONNECT",6)==0){
          ret = M_CONNECT;
        }
        break;
    case 'O': //OPTIONS
        if(strncmp(requete+1,"PTIONS",6)==0){
          ret = M_OPTIONS;
        }
        break;
    case 'T': //TRACE
        if(strncmp(requete+1,"RACE",4)==0){
          ret = M_TRACE;
        }
        break;
    case 'D': //DELETE
        if(strncmp(requete+1,"ELETE",5)==0){
          ret = M_DELETE;
        }
        break;
    case 'P': // PUT , POST , PATCH
        if(strncmp(requete+1,"OST",3)==0){
          ret = M_POST;
        }else if(strncmp(requete+1,"UT",2)==0){
          ret = M_PUT;
        }else if(strncmp(requete+1,"ATCH",4)==0){
          ret = M_PATCH;
        }
        break;
    case 'G': // GET
        if(strncmp(requete+1,"ET",2)==0){
          ret = M_GET;
        }
    default:
        break; // aucune Méthode ne correspond
    }
    return ret;
}

int extraitFichier(char *requete, char *tableauNomFichier, int tailleTableauNomFichier) {
    char *buf = requete;
    int ret = 0;
    buf = strchr(buf,'/');
    if (buf == NULL) {
        goto on_error;
    }
    buf++;
    if(*buf==' '){
        strncpy(tableauNomFichier,"index.html",11); // on utilise strncpy et non pas strcpy pour protéger des failles de sécurite
        ret = 1;
        return ret;
    }else{
        char *save = buf;
        buf = strchr(save,' ');
        if(buf == NULL){
            goto on_error;
        }
        size_t taille = (buf-save)*sizeof(char);
        strncpy(tableauNomFichier,save,taille);
        tableauNomFichier[taille]='\0';
    }
    return ret;
    on_error:
    perror("Mauvais formatage de la requete");
    return ret;
}
//TODO: Factorisation possible
int envoyerReponse200HTML(char *nomFichier){
  return envoyerReponse200Texte(nomFichier,"text/html");
}

int envoyerReponse200JPG(char *nomFichier){
  return envoyerReponse200Binaire(nomFichier,"image/jpeg");
}

int envoyerReponse200ICO(char *nomFichier){
  return envoyerReponse200Binaire(nomFichier,"image/x-icon");
}

int envoyerReponse200Texte(char *nomFichier, char *mimeType){
  size_t taille = taille_fichier(nomFichier);
  envoyerHeaderReponse("200 OK",mimeType,taille);
  return envoyerContenuFichierTexte(nomFichier);
}

int envoyerReponse200Binaire(char *nomFichier, char *mimeType){
  size_t taille = taille_fichier(nomFichier);
  envoyerHeaderReponse("200 OK",mimeType,taille);
  return envoyerContenuFichierBinaire(nomFichier);
}

int envoyerReponse404HTML(char *nomFichier){
  char *contentFormat = "<html><head></head><body><h1>404 Not Found</h1><pre>Le chemin : %s n'existe pas</pre></body></html>";
  size_t size = (strlen(contentFormat)-2+strlen(nomFichier)+1)*sizeof(char);
  char *content = malloc(size);
  sprintf(content,contentFormat,nomFichier);
  envoyerHeaderReponse("404 Not Found","text/html",size);
  return EmissionBinaire(content,size);
}
int envoyerReponse500HTML(const char *message){
  char *contentFormat = "<html><head></head><body><h1>500 Server Error</h1><pre>Raison : %s</pre></body></html>";
  size_t size = (strlen(contentFormat)-2+strlen(message)+1)*sizeof(char);
  char *content = malloc(size);
  sprintf(content,contentFormat,message);
  envoyerHeaderReponse("500 Server Error","text/html",size);
  return EmissionBinaire(content,size);
}

int envoyerHeaderReponse(const char *code,const char *type,const unsigned long size){
  int ret = -1;
  const char *head[] = {"HTTP/1.1 %s","Content-type: %s; charset=UTF-8","Content-length: %lu"};
  unsigned long nDigits = floor(log10(abs(size))) + 1; //on obtient le nombre de caractères de l'entier
  char *headerFormat = build_message(head,3); // on construit le message
  if(headerFormat == NULL) goto free_error;
  //on alloue la taille finale de la chaine, càd on retire les %s et %d et on ajoute le code + le type + \0
  char *header = (char*) malloc((strlen(headerFormat)-7+strlen(code)+strlen(type)+nDigits+1)*sizeof(char));
  if (header == NULL) goto free_error;
  ret = sprintf(header,headerFormat,code,type,size);
  Emission(header);
  free_error:
  if(headerFormat == NULL || header == NULL) fprintf(stderr,"Erreur lors de la construction de la requête");
  if(header != NULL) free(header);
  if(headerFormat != NULL) free(headerFormat);
  return ret;
}

int envoyerContenuFichierBinaire(char *nomFichier){
  return envoyerContenuFichier(nomFichier,1);
}

int envoyerContenuFichierTexte(char *nomFichier){
  return envoyerContenuFichier(nomFichier,0);
}

//envoie un fichier, binaire ou non avec le paramètre binary
int envoyerContenuFichier(char *nomFichier,int binary){
  int ret = 1;
  FILE *f = fopen(nomFichier,binary==0?"r":"rb");
  size_t taille = taille_fichier(nomFichier); 
  char *fileContent=(char*)malloc(taille);//on alloue toute la taille du fichier demandé
  if (f == NULL){
    goto on_error_free;
  }
  // On place le curseur en début de fichier
  fseek(f,0,SEEK_SET);
  // On lit tout le ontenu du fichier
  fread(fileContent, taille, 1, f);
  // On envoie tout le contenu au client
  ret = EmissionBinaire(fileContent, taille);
  on_error_free:
  if(f == NULL)fprintf(stderr, "Erreur lors de l'ouverture du fichier\n");
  else fclose(f);
  return ret;
}

size_t taille_fichier(char *filename) {
  size_t result = -1;
  FILE *file = fopen(filename, "r");
  if (file == NULL)
    goto free_error;
  fseek(file, 0, SEEK_END);
  result = (size_t)ftell(file);
free_error:
  if (file != NULL)
    fclose(file);
  return result;
}

// crée un message d'un nombre de ligne défini
char *build_message(const char *lines[],int nblines){
	size_t size = 3;
	char *message = (char*)malloc(size);
  char *line = NULL;
	if(!message)
	  goto on_error_free;
	*message='\0';
	for(int i = 0; i<nblines;i++){
		line = build_line(lines[i]);
    if(line == NULL) goto on_error_free;
		size_t sline = strlen(line);
		size+=sline;
		message = (char*)realloc(message,size);
		message = strncat(message,line,sline);
		free(line);
    line = NULL;
	}
  message[size-3]='\r';
  message[size-2]='\n';
  message[size-1]='\0';
  on_error_free:
  if(line != NULL) {
    free(message);
    message = NULL;
    free(line);
  } 
	return message;
}

char *build_line(const char* line){
  size_t sline = strlen(line);
	/*char *esc = (char*)line+sline;
	while(*(esc--) == '\n'){ // removing \n at end
		esc = '\0';
	}*/
	char *newline = (char*)malloc(sline+4*sizeof(char));
	if(newline == NULL)
		return NULL;
  *newline = '\0';
	newline = strncpy(newline,line,sline);
  newline[sline] = '\0';
	newline = strncat(newline,"\r\n",3);
  newline[sline+3] = '\0';
	return newline;
}