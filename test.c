#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int extraitFichier(char *requete, char *tableauNomFichier, int tailleTableauNomFichier) {
    char *buf = requete;
    int ret = 0;
    buf = strchr(buf,'/');
    if (buf == NULL) {
        goto on_error;
    }
    buf++;
    if(*buf==' '){
        strncpy(tableauNomFichier,"index.html",11); // on utilise strncpy et non pas strcpy pour protéger des failles de sécurite
        ret = 1;
        return ret;
    }else{
        char *save = buf;
        buf = strchr(save,' ');
        if(buf == NULL){
            goto on_error;
        }
        size_t taille = (buf-save)*sizeof(char);
        strncpy(tableauNomFichier,save,taille);
        tableauNomFichier[taille]='\0';
    }
    return ret;
    on_error:
    perror("Mauvais formatage de la requete");
    return ret;
}

size_t taille_fichier(char *filename) {
  size_t result = -1;
  FILE *file = fopen(filename, "r");
  if (file == NULL)
    goto free_error;
  fseek(file, 0, SEEK_END);
  result = (size_t)ftell(file);
free_error:
  if (file != NULL)
    fclose(file);
  return result;
}

int main(){
    char tab[50];
    extraitFichier("GET /test.c HTTP/1.1",tab,20);
    printf("nom du fichier depuis la requete : %s\n",tab);
    printf("taille du fichier de la requête : %lu\n",taille_fichier(tab));
}