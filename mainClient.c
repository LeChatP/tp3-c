#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client.h"
char *build_message(char *lines[],int nblines);
char *build_line(const char*);
int main() {
	int code = 1;
	char *message;

	if(InitialisationAvecService("info.cern.ch","80") != 1) {
		printf("Erreur d'initialisation\n");
		return 1;
	}
	char *get[2] = {"GET /hypertext/WWW/TheProject.html HTTP/1.1","Host: info.cern.ch"};
	message = build_message(get,sizeof(get)/sizeof(char **));
	if(message == NULL) goto free_error;
	Emission(message);
	free(message);
	while((message = Reception()) != NULL){
		printf("%s",message);
		free(message);
	}
	Terminaison();
	code = 0;
	free_error:
	if(message != NULL) free(message);
	return code;
}

char *build_message(char *lines[],int nblines){
	size_t size = 2;
	char *message = (char*)malloc(size);
	if(!message)
		return NULL;
	*message='\0';
	for(int i = 0; i<nblines;i++){
		char *line = build_line(lines[i]);
		size_t sline = strlen(line);
		size+=sline;
		message = realloc(message,size);
		message = strncat(message,line,sline);
		free(line);
	}
	message[size-2]='\r';
	message[size-1]='\n';
	return message;
}
char *build_line(const char* line){
	char *esc = NULL;
	while((esc = strchr(line,'\n'))!=NULL){
		esc = '\0';
	}
	char *newline = (char*)malloc(strlen(line)+3);
	if(!newline)
		return NULL;
	newline = strncpy(newline,line,strlen(line));
	newline = strncat(newline,"\r\n",4);
	return newline;
}